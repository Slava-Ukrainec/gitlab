let modalMenu = document.getElementById("headerNavigation");
let menuButton = document.getElementById("menuIcon");

window.addEventListener('resize', function(event){
        if (window.innerWidth > 320) {
            if (document.querySelectorAll('[href="css/media.css"]').length == 0) {
                let link = document.createElement('link');
                link.rel = 'stylesheet';
                link.href = 'css/media.css';
                document.querySelector('head').appendChild(link);
            }
        }
        if (window.matchMedia("(min-width: 640px)").matches) {
            modalMenu.style.display = "flex"
        } else {
            modalMenu.style.display = "none"
            menuButton.innerHTML = '<i class="fas fa-bars"></i>'
        }

});

//menuButton.addEventListener('mouseover', function(event){ // mouseover is a very strange solution for mobile devices that have no cursors
//            modalMenu.style.display = "block";
//            menuButton.innerHTML = '<i class="fas fa-times"></i>'
//});
//
//modalMenu.addEventListener('mouseout', function(event){
//            modalMenu.style.display = "none";
//            menuButton.innerHTML = '<i class="fas fa-bars"></i>'
//});



document.addEventListener('click', function(event){
    if (event.target.closest('.menu-icon')) {
        if (modalMenu.style.display !== "block") {
            modalMenu.style.display = "block";
            menuButton.innerHTML = '<i class="fas fa-times"></i>'
        }
        else {
            modalMenu.style.display = "none";
            menuButton.innerHTML = '<i class="fas fa-bars"></i>'
        }
    }
    else if(modalMenu.style.display == "block" && !event.target.closest('.header-navigation-list')) {
        modalMenu.style.display = "none";
        menuButton.innerHTML = '<i class="fas fa-bars"></i>'
        }

});
